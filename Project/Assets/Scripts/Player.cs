using UnityEngine;
using System;
using System.Collections;
/// <summary>
/// Local player class
/// </summary>
public class Player{
	/// <summary>
	/// Positions.
	/// </summary>
	public enum Positions
	{
		Member 		= 0,
		Chief		= 1,
		Banker		= 2,
		MainBuilder = 3,
		Strategist 	= 4
	}
	
	public int Id { get; set; }
    public string Name { get; set;}
	public Positions Position { get; set;}
	public int VillageId { get; set;}
	/// <summary>
	/// Gets int value of the Position
	/// </summary>
	/// <value>
	/// Int
	/// </value>
	public int NumericalPosition 
	{
		get{
			return (int)Position;
		}
	}
	
}
