using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Class handling GUI events
/// </summary>
public class VillagePosition : MonoBehaviour {
	
	public UIPanel mainPanel, popupPanel, addPlayerPanel,
	removePlayerPanel, editPlayerPanel, messagePanel;
	
	public UIButton manageButton, messageOkButton, addPlayerAcceptButton,
	addPlayerCancelButton, addPlayerButton, removePlayerButton,
	removePlayerAcceptButton, removePlayerCancelButton, editPlayerButton,
	editPlayerAcceptButton, editPlayerCancelButton;
	
	public UIInput playerInput;
	public UILabel editPlayerLabel, messageLabel, messageTitleLabel, removePlayerMessageLabel;
	public UIPopupList detailsList, playersList, positionList;
	
	public int UserId;
	private Village _village;
	
	void Start () {
		TestConnection();
		
		#region Button handlers
			UIEventListener.Get(manageButton.gameObject).onClick += ManageButtonOnClick;
			UIEventListener.Get(addPlayerButton.gameObject).onClick += AddPlayerButtonOnClick;
			UIEventListener.Get(editPlayerButton.gameObject).onClick += EditPlayerButtonOnClick;
			UIEventListener.Get(removePlayerButton.gameObject).onClick += RemovePlayerButtonOnClick;
			
			UIEventListener.Get(addPlayerAcceptButton.gameObject).onClick += AddPlayerAcceptButtonOnClick;
			UIEventListener.Get(addPlayerCancelButton.gameObject).onClick += AddPlayerCancelButtonOnClick;
			
			UIEventListener.Get(removePlayerAcceptButton.gameObject).onClick += RemovePlayerAcceptButtonOnClick;
			UIEventListener.Get(removePlayerCancelButton.gameObject).onClick += RemovePlayerCancelButtonOnClick;
			
			UIEventListener.Get(editPlayerAcceptButton.gameObject).onClick += EditPlayerAcceptButtonOnClick;
			UIEventListener.Get(editPlayerCancelButton.gameObject).onClick += EditPlayerCancelButtonOnClick;
			
			UIEventListener.Get(messageOkButton.gameObject).onClick += MessageOkButtonOnClick;
		#endregion
		
		playersList.onSelectionChange += PlayerListOnSelectionChanged;
		NGUITools.SetActive(mainPanel.gameObject,true);
	}
	
	private void TestConnection()
	{
		ServerConnection.Instance.TestConnection(connection => ProcessTestConnection(connection));
	}
	
	private void ProcessTestConnection(bool connection)
	{
		if(connection)
		{
			GetVillage();
		}
		else
		{
			messageTitleLabel.text = "Error";
			messageLabel.text = "Cannot connect to the Server";
			NGUITools.SetActive(messagePanel.gameObject, true);
		}
	}
	
	
	/// <summary>
	/// Local copy of village
	/// </summary>
	private void GetVillage()
	{
		ServerConnection.Instance.GetVillage(village => ProcessVillage(village), UserId.ToString());
	}
	//Process async request
	private void ProcessVillage(Village village)
	{
		if(village.Name == null)
		{
			messageTitleLabel.text = "Error";
			messageLabel.text = "Cannot connect to the Village";
			NGUITools.SetActive(messagePanel.gameObject, true);
		}
		else
		{
		_village = village;
		//Handling server responses
		_village.OnResponse += MessageHandler;
		_village.GetPlayers(PlayerListHandler);
		}
	}
	
	void UpdatePopupList(UIPopupList list, List<string> items)
	{
		list.items = items;
	}
	
	//set available positions to list
	void SetPositions()
	{
		List<string> list = new List<string>();
        foreach(string position in Enum.GetNames(typeof(Player.Positions)))
        {
                list.Add(position);
        }
		UpdatePopupList(positionList, list);
	}
	
	//set details on selection change
	void PlayerListOnSelectionChanged(string selection)
	{
		PlayerListHandler();
		Player p = _village.GetPlayer(selection);
		if(p != null)
		{
		List<string> list = new List<string>();
		list.Add(p.Position.ToString());
		detailsList.items = list;
		}
	}
	/// <summary>
	/// Shows message dialog
	/// </summary>
	/// <param name='message'>
	/// Message.
	/// </param>
	public void MessageHandler(Message message)
	{
			messageTitleLabel.text = message.Type.ToString();
			messageLabel.text = message.Text;
			NGUITools.SetActive(messagePanel.gameObject, true);
			//In case that player position was updated
			PlayerListOnSelectionChanged(playersList.selection);
	}
	/// <summary>
	/// Updates player list when changed
	/// </summary>
	void PlayerListHandler()
	{
		UpdatePopupList(playersList, _village.players.ToNameList());
	}
	
	// Toogle Popup panel on click
	void ManageButtonOnClick(GameObject GO)
	{
		bool state;
		
		if(popupPanel.gameObject.activeSelf)
			state = false;
		else
			state = true;
		NGUITools.SetActive(popupPanel.gameObject, state);
	}
	void DisableColliders()
	{
			//Disallow player to change selection
			playersList.collider.enabled = false;
	}
	void EnableColliders()
	{
			playersList.collider.enabled = true;
	}
	
	void AddPlayerButtonOnClick(GameObject GO)
	{
		DisableColliders();
		NGUITools.SetActive(addPlayerPanel.gameObject,true);
	}
	
	void EditPlayerButtonOnClick(GameObject GO)
	{
		DisableColliders();
		SetPositions(); // set position list
		editPlayerLabel.text = playersList.selection;
		NGUITools.SetActive(editPlayerPanel.gameObject,true);
	}
	
	void RemovePlayerButtonOnClick(GameObject GO)
	{
		DisableColliders();
		removePlayerMessageLabel.text = "Do yo want to remove " + playersList.selection + "?";
		NGUITools.SetActive(removePlayerPanel.gameObject,true);
	}
	
	void AddPlayerAcceptButtonOnClick(GameObject GO)
	{
		EnableColliders();
		string input = playerInput.text; // player name entered by user
		_village.AddPlayer(input, UserId.ToString());
		
		playerInput.text = playerInput.defaultText;
		NGUITools.SetActive(addPlayerPanel.gameObject, false);
	}
	
	void AddPlayerCancelButtonOnClick(GameObject GO)
	{
		EnableColliders();
		NGUITools.SetActive(addPlayerPanel.gameObject, false);
		playerInput.text = playerInput.defaultText;
	}
	
	void RemovePlayerAcceptButtonOnClick(GameObject GO)
	{
		EnableColliders();
		_village.RemovePlayer(playersList.selection,UserId.ToString());
		NGUITools.SetActive(removePlayerPanel.gameObject, false);
		playersList.collider.enabled = true;
	}
	
	void RemovePlayerCancelButtonOnClick(GameObject GO)
	{
		EnableColliders();
		NGUITools.SetActive(removePlayerPanel.gameObject, false);
		playersList.collider.enabled = true;
	}
	
	void EditPlayerAcceptButtonOnClick(GameObject GO)
	{
		EnableColliders();
		//if position exist
		try{
		Player.Positions position = (Player.Positions)Enum.Parse(typeof(Player.Positions), positionList.selection);
		_village.EditPlayer(editPlayerLabel.text,position,UserId.ToString());
		}
		catch
		{
		Message m = new Message(Message.Types.Error, "Position not found");
		MessageHandler(m);
		}
		NGUITools.SetActive(editPlayerPanel.gameObject, false);
	}
	
	void EditPlayerCancelButtonOnClick(GameObject GO)
	{
		EnableColliders();
		NGUITools.SetActive(editPlayerPanel.gameObject, false);
	}
	
	void MessageOkButtonOnClick(GameObject GO)
	{
		EnableColliders();
		NGUITools.SetActive(messagePanel.gameObject, false);
	}
}
