using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Class containing players
/// </summary>
public class Players {
	public event ChangeHandler OnChange;
    public delegate void ChangeHandler();
	
	Dictionary<string,Player> dictionary;
	/// <summary>
	/// Initializes a new instance of the <see cref="Players"/> class.
	/// </summary>
	public Players()
	{
		dictionary = new Dictionary<string,Player>();
	}
	/// <summary>
	/// Initializes a new instance of the <see cref="Players"/> class.
	/// </summary>
	/// <param name='list'>
	/// List<player>
	/// </param>
	public Players(List<Player> list)
	{
		dictionary = new Dictionary<string,Player>();
		foreach(Player p in list)
		{
			dictionary.Add(p.Name, p);
		}
	}
	/// <summary>
	/// Initializes a new instance of the <see cref="Players"/> class.
	/// </summary>
	/// <param name='array'>
	/// Player[]
	/// </param>
	public Players(Player[] array)
	{
		dictionary = new Dictionary<string,Player>();
		foreach(Player p in array)
		{
			dictionary.Add(p.Name, p);
		}
	}
	/// <summary>
	/// Adds the player.
	/// </summary>
	/// <param name='player'>
	/// Player
	/// </param>
	public void AddPlayer(Player player)
	{
		dictionary.Add(player.Name, player);
		OnChange();
	}
	/// <summary>
	/// Gets the player.
	/// </summary>
	/// <returns>
	/// The player.
	/// </returns>
	/// <param name='name'>
	/// String
	/// </param>
	public Player GetPlayer(string name)
	{
		if(dictionary.ContainsKey(name))
		return dictionary[name];
		else
		return null;
	}
	/// <summary>
	/// Edits the player.
	/// </summary>
	/// <param name='player'>
	/// Player.
	/// </param>
	public void EditPlayer(Player player)
	{
		dictionary[player.Name] = player;
		OnChange();
	}
	/// <summary>
	/// Removes the player.
	/// </summary>
	/// <param name='player'>
	/// Player.
	/// </param>
	public void RemovePlayer(Player player)
	{
		dictionary.Remove(player.Name);
		OnChange();
	}
	/// <summary>
	/// Removes the player.
	/// </summary>
	/// <param name='playerName'>
	/// String
	/// </param>
	public void RemovePlayer(string playerName)
	{
		dictionary.Remove(playerName);
		OnChange();
	}
	/// <summary>
	/// Convert to list
	/// </summary>
	/// <returns>
	/// Player list
	/// </returns>
	public List<Player> ToList()
	{
		List<Player> _temp = new List<Player>();
		foreach(KeyValuePair<string, Player> entry in dictionary){
			_temp.Add(entry.Value);
		}
		return _temp;
	}
	/// <summary>
	/// Convert to names list
	/// </summary>
	/// <returns>
	/// List<string>
	/// </returns>
	public List<string> ToNameList()
	{
		List<string> _temp = new List<string>();
		foreach(KeyValuePair<string, Player> entry in dictionary){
			_temp.Add(entry.Value.Name);
		}
		return _temp;
	}	
	/// <summary>
	/// Convert to array
	/// </summary>
	/// <returns>
	/// Player[]
	/// </returns>
	public Player[] ToArray()
	{
		Player[] _temp = new Player[dictionary.Count];
		dictionary.Values.CopyTo(_temp, 0);
		return _temp;
	}
}
