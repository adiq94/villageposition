using UnityEngine;
using System.Collections;
/// <summary>
/// Class transporting message to GUI
/// </summary>
public class Message{
	/// <summary>
	/// Type of message
	/// </summary>
	public enum Types
	{
		Information,
		Warning,
		Error
	}
	public Types Type 	{get; set;}
	public string Text	{get; set;}
	/// <summary>
	/// Initializes a new instance of the <see cref="Message"/> class.
	/// </summary>
	/// <param name='type'>
	/// Type
	/// </param>
	/// <param name='text'>
	/// String
	/// </param>
	public Message(Types type, string text)
	{
		Type = type;
		Text = text;
	}

}
