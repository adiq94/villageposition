using UnityEngine;
using System.Collections;
/// <summary>
/// Class storing response from server
/// </summary>
public class JsonMessage {
	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="JsonMessage"/> is success.
	/// </summary>
	/// <value>
	/// <c>true</c> if success; otherwise, <c>false</c>.
	/// </value>
    public bool Success { get; set; }
	/// <summary>
	/// Gets or sets the message.
	/// </summary>
	/// <value>
	/// The message.
	/// </value>
    public string Message { get; set; }
}
