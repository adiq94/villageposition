using UnityEngine;
using System.Collections;
/// <summary>
/// Local village class
/// </summary>
public class Village{
	
	public int Id;
	public string Name;
	/// <summary>
	/// The collection of Player
	/// </summary>
	public Players players;
	/// <summary>
	/// Allows communication with server
	/// </summary>
	ServerConnection server;
	
	private Message _response;
	/// <summary>
	/// Occurs when server response received
	/// </summary>
	public event ResponseHandler OnResponse;
	/// <summary>
	/// Response handler.
	/// </summary>
    public delegate void ResponseHandler(Message response);
	
	/// <summary>
	/// Gets or sets the server response.
	/// </summary>
	/// <value>
	/// Message
	/// </value>
	public Message Response{
		get{return _response;}
		set
		{
			_response = value;
			OnResponse(_response);
		}
	}
	/// <summary>
	/// Initializes a new instance of the <see cref="Village"/> class.
	/// </summary>
	public Village()
	{
		server = ServerConnection.Instance;
		players = new Players();
	}
	/// <summary>
	/// Get local copy of players
	/// </summary>
	/// <param name='handler'>
	/// Handler notifying about changes of Players
	/// </param>
	public void GetPlayers(Players.ChangeHandler handler)
	{
		ServerConnection.Instance.GetPlayers(callback => ProcessGetPlayers(callback, handler), Id.ToString());
	}

	private void ProcessGetPlayers(Player[] playerArray, Players.ChangeHandler handler)
	{		
			players = new Players(playerArray);
			players.OnChange += handler;
	}
	
	/// <summary>
	/// Gets the player.
	/// </summary>
	/// <returns>
	/// The player.
	/// </returns>
	/// <param name='name'>
	/// String
	/// </param>
	public Player GetPlayer(string name)
	{
		return players.GetPlayer(name);
	}	
	/// <summary>
	/// Adds the player.
	/// </summary>
	/// <param name='name'>
	/// String
	/// </param>
	/// <param name='currentUserId'>
	/// Current user identifier.
	/// </param>
	public void AddPlayer(string name, string currentUserId)
	{
		Player player = new Player();
		//set initial values for added player
		player.Name = name;
		player.Position = Player.Positions.Member;
		player.VillageId = Id;		
		
		server.AddPlayer(callback => ProcessAddPlayer(callback, player), player, currentUserId);
	}
	
	private void ProcessAddPlayer(JsonMessage message, Player player)
	{
		if(message.Success){		
			players.AddPlayer(player);
			Message m = new Message(Message.Types.Information, "Player added");
			Response = m;
		}
		else if(message.Message.Equals("Player doesn't exist")){
			Message m = new Message(Message.Types.Warning, "Player not found");
			Response = m;
		}
		
		else if(message.Message.Equals("Player already has village")){
			Message m = new Message(Message.Types.Warning, "Player already has village");
			Response = m;
		}
		else if(!message.Success){
			Message m = new Message(Message.Types.Error, "Error, cannot add player");
			Response = m;
		}
	}
	/// <summary>
	/// Edits the player.
	/// </summary>
	/// <param name='name'>
	/// String
	/// </param>
	/// <param name='position'>
	/// Position.
	/// </param>
	/// <param name='currentUserId'>
	/// Current user identifier.
	/// </param>
	public void EditPlayer(string name, Player.Positions position, string currentUserId)
	{
		Player player = new Player();
		//set initial values for added player
		player.Name = name;
		player.Position = position;
		player.VillageId = Id;	
		
		server.EditPlayerPosition(callback => ProcessEditPlayer(callback, player), player, currentUserId);
	}
	
	private void ProcessEditPlayer(JsonMessage message, Player player)
	{
		if(message.Success){		
			players.EditPlayer(player);
			Message m = new Message(Message.Types.Information, "Player edited");
			Response = m;
		}
		else if(message.Message.Equals("Cannot edit position of this player")){
			Message m = new Message(Message.Types.Error, "Cannot edit position of this player");
			Response = m;
		}
		
		else if(message.Message.Equals("You cannot grant this position")){
			Message m = new Message(Message.Types.Error, "You cannot grant this position");
			Response = m;
		}
		else if(!message.Success){
			Message m = new Message(Message.Types.Error, "Error, cannot edit player");
			Response = m;
		}
	}
	/// <summary>
	/// Removes the player.
	/// </summary>
	/// <param name='name'>
	/// String
	/// </param>
	/// <param name='currentUserId'>
	/// Current user identifier.
	/// </param>
	public void RemovePlayer(string name, string currentUserId)
	{
		Player player = new Player();
		//set initial values for added player
		player.Name = name;
		player.VillageId = Id;	
		
		server.RemovePlayer(callback => ProcessRemovePlayer(callback, player), player, currentUserId);
	}
	
	private void ProcessRemovePlayer(JsonMessage message, Player player)
	{
		if(message.Success){		
			players.RemovePlayer(player);
			Message m = new Message(Message.Types.Information, "Player removed");
			Response = m;
		}
		
		else if(message.Message.Equals("This player doesn't belong to this village")){
			Message m = new Message(Message.Types.Error, "This player doesn't belong to this village");
			Response = m;
		}
		
		else if(message.Message.Equals("Cannot remove this playere")){
			Message m = new Message(Message.Types.Error, "Cannot remove this player");
			Response = m;
		}
		
		else if(!message.Success){
			Message m = new Message(Message.Types.Error, "Error, cannot remove player");
			Response = m;
		}
	}
}
