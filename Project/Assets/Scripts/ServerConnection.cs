using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Server connection class
/// </summary>
public class ServerConnection : MonoBehaviour
{
	#region Server connection settings
	/// <summary>
	/// The server URL.
	/// </summary>
	string serverUrl = "http://localhost:1337";
	/// <summary>
	/// The private key.
	/// </summary>
	string privateKey = "testPrivateKey";
	#endregion
	
	private static ServerConnection instance;
 
	public static ServerConnection Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new GameObject ("ServerConnection").AddComponent<ServerConnection> ();
			}
 
			return instance;
		}
	}
 
	public void OnApplicationQuit ()
	{
		instance = null;
	}
	/// <summary>
	/// Tests the connection.
	/// </summary>
	/// <param name='callback'>
	/// Action<bool> Callback.
	/// </param>
	public void TestConnection (Action<bool> callback)
	{
		string url = "/testConnection";
		StartCoroutine(GetConnection(url, callback));
	}
	
	/// <summary>
	/// Calculates the Hmac using SHA-1
	/// </summary>
	/// <returns>
	/// String
	/// </returns>
	/// <param name='publicKey'>
	/// Public key.
	/// </param>
	/// <param name='privateKey'>
	/// Private key.
	/// </param>
	string CalculateHmac(string publicKey, string privateKey)
	{
		byte[] keyArray = Encoding.ASCII.GetBytes(privateKey);
        HMACSHA1 myhmacsha1 = new HMACSHA1(keyArray);
        byte[] byteArray = Encoding.ASCII.GetBytes( publicKey );
        MemoryStream stream = new MemoryStream( byteArray ); 
        byte[] hashValue = myhmacsha1.ComputeHash(stream);
        return string.Join("", Array.ConvertAll(hashValue, b => b.ToString("x2"))); // convert to string
	}
	/// <summary>
	/// Adds the player.
	/// </summary>
	/// <param name='callback'>
	/// Action<JsonMessage> Callback.
	/// </param>
	/// <param name='player'>
	/// targetName,villageId,position
	/// </param>
	/// <param name='currentUserId'>
	/// Current user identifier.
	/// </param>
	public void AddPlayer (Action<JsonMessage> callback, Player player, string currentUserId )
	{
		string url = "/village/addPlayer";
		Dictionary<string, string> dic = new Dictionary<string, string>();
		dic.Add("sourceUserId", currentUserId);
		dic.Add("targetName", player.Name);
		dic.Add("villageId", player.VillageId.ToString());
		dic.Add("position", player.NumericalPosition.ToString());
		string key = currentUserId + player.Name + player.VillageId.ToString() + player.NumericalPosition.ToString();
		dic.Add("requestToken", CalculateHmac(key, privateKey));
		StartCoroutine(PostRequest(url, dic, callback));
	}
	/// <summary>
	/// Removes the player.
	/// </summary>
	/// <param name='callback'>
	/// Action<JsonMessage> Callback.
	/// </param>
	/// <param name='player'>
	/// targetName
	/// </param>
	/// <param name='currentUserId'>
	/// Current user identifier.
	/// </param>
	public void RemovePlayer (Action<JsonMessage> callback, Player player, string currentUserId)
	{
		string url = "/village/removePlayer";
		Dictionary<string, string> dic = new Dictionary<string, string>();
		dic.Add("sourceUserId", currentUserId);
		dic.Add("targetName", player.Name);
		string key = currentUserId + player.Name;
		dic.Add("requestToken", CalculateHmac(key, privateKey));
		StartCoroutine(PostRequest(url, dic, callback));
	}
	/// <summary>
	/// Edits the player position.
	/// </summary>
	/// <param name='callback'>
	/// Action<JsonMessage> Callback.
	/// </param>
	/// <param name='player'>
	/// targetName,villageId,position
	/// </param>
	/// <param name='currentUserId'>
	/// Current user identifier.
	/// </param>
	public void EditPlayerPosition (Action<JsonMessage> callback, Player player, string currentUserId)
	{
		string url = "/village/setPosition";
		Dictionary<string, string> dic = new Dictionary<string, string>();
		dic.Add("sourceUserId", currentUserId);
		dic.Add("targetName", player.Name);
		dic.Add("villageId", player.VillageId.ToString());
		dic.Add("position", player.NumericalPosition.ToString());
		string key = currentUserId + player.Name + player.VillageId.ToString() + player.NumericalPosition.ToString();
		dic.Add("requestToken", CalculateHmac(key, privateKey));
		StartCoroutine(PostRequest(url, dic, callback));
	}
	/// <summary>
	/// Gets the players.
	/// </summary>
	/// <param name='callback'>
	/// Action<Player[]> Callback.
	/// </param>
	/// <param name='villageId'>
	/// Village identifier.
	/// </param>
	public void GetPlayers (Action<Player[]> callback, string villageId)
	{
		string url = string.Format("/village/getPlayers?villageId={0}", villageId);
		StartCoroutine(GetPlayers(url, callback));
	}
	/// <summary>
	/// Gets the village.
	/// </summary>
	/// <param name='callback'>
	/// Action<Village> Callback.
	/// </param>
	/// <param name='sourceUserId'>
	/// Source user identifier.
	/// </param>
	public void GetVillage (Action<Village> callback, string sourceUserId)
	{
		string url = string.Format("/village/getVillage?sourceUserId={0}", sourceUserId);
		StartCoroutine(GetVillage(url, callback));
	}
	
		
	IEnumerator PostRequest (string apiUrl, Dictionary<string,string> parameters, Action<JsonMessage> callback)
	{
		// Create a form object for sending POST request to the server
		var form = new WWWForm ();
		// Server Url + specific API function
		string adress = serverUrl + apiUrl;
		
		foreach (KeyValuePair<string, string> keyValue in parameters) {
			form.AddField (keyValue.Key, keyValue.Value);
		}
		
		var download = new WWW (adress, form);
		// Wait until the download is done
		yield return download;
		
		if (download.error != null) {
			JsonMessage _temp = new JsonMessage();
			_temp.Success = false;
			_temp.Message = download.error;
			callback(_temp);
		} else {
			//Parse response
			JsonMessage response = LitJson.JsonMapper.ToObject<JsonMessage>(download.text);
			callback(response);
		}
	}
	
	IEnumerator GetPlayers (string apiUrl, Action<Player[]> callback)
	{
		string adress = serverUrl + apiUrl;
		// Create a download object
		var download = new WWW (adress);
		// Wait until the download is done
		yield return download;
		
		if (download.error != null) {
			callback(null);
		} else {
			Player[] result = LitJson.JsonMapper.ToObject<Player[]> (download.text);
			callback(result);
		}
	}
	
	IEnumerator GetVillage (string apiUrl, Action<Village> callback)
	{
		string adress = serverUrl + apiUrl;
		// Create a download object
		var download = new WWW (adress);
		// Wait until the download is done
		yield return download;
		
		if (download.error != null) {
			callback(null);
		} else {
			//Parse response
			Village response = LitJson.JsonMapper.ToObject<Village>(download.text);
			callback(response);
		}
	}
	
	IEnumerator GetConnection (string apiUrl, Action<bool> callback)
	{
		string adress = serverUrl + apiUrl;
		// Create a download object
		var download = new WWW (adress);
		// Wait until the download is done
		yield return download;
		
		if (download.error != null) {
			callback(false);
		} else {
			callback(true);
		}
	}
}
