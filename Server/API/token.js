var calculateHMAC = function (key) {

	var publicKey = key; // Key must be string
	var privateKey = "testPrivateKey";

	//calculate HMAC, use SHA-1
	var crypto = require('crypto')
	, text = publicKey
	, key = privateKey
	, hash
	hash = crypto.createHmac('sha1', key).update(text).digest('hex');

	return hash;
}

module.exports.calculateHMAC = calculateHMAC;