var express = require('express');
var app = express();
mongo = require('mongojs').connect('localhost:27017/game', ['villages', 'players']);
var token = require('./token') // Attach token.js

app.listen(1337, function() {
	console.log("Application listening on port: 1337");
});

app.use(express.bodyParser()); // Necessary for POST data parse in Express 3+


function success(success)
{
	return { "Success": success };
}
function success(success, message)
{
	return { "Success": success, "Message": message };
}

function getPlayerByName (name, callback) {
	//Find player by name
	mongo.players.findOne({"Name": name}, function(err, result) {
		if (err || !result){
			callback(null); // Name doesn't exist
		}

		else
		{
			callback(result);
		}
	});
}

function getPlayerById(id, callback)
{
	//Find player by id
	mongo.players.findOne({"Id": id},  function(err, result) {
		if (err || !result){
			callback(null); // Id doesn't exist
		}

		else
		{
			callback(result);
		}
	});
}


app.post('/village/addPlayer', function(req, res) {
	var sourceUserId	= parseInt(req.param('sourceUserId'));
	var targetName		= req.param('targetName');
	var villageId      	= parseInt(req.param('villageId'));
	var position 		= parseInt(req.param('position'));
	var requestToken	= req.param('requestToken');

	if (sourceUserId && targetName && villageId && position.toString() && requestToken) {
		var _token = token.calculateHMAC(sourceUserId.toString()+targetName+villageId.toString()+position.toString());
	}
	else {
		res.send(success(false, "Incomplete request"));
		return;}	

	getPlayerByName(targetName, function(targetUserId) //asynchronous call, not sure if there's more neat way to retrieve variable in MongoJS
	{
		if(!targetUserId)
		{
			res.send(success(false, "Player doesn't exist"));
			return;}
			else
				getPlayerById(sourceUserId, function(sourcePlayer){

					if(sourcePlayer.VillageId != villageId){
						res.send(success(false, "Incorrect village"));
						return;
					}

					if(targetUserId.VillageId){
						res.send(success(false, "Player already has village"));
						return;
					}

					if(position == 1){
						res.send(success(false, "You cannot grant this position"));
						return;
					}	

					if(_token == requestToken){
						mongo.players.update({"Id": targetUserId.Id}, { $set: { "VillageId": villageId, "Position": position } }, function(err, result) {
							if (err || !result) {
								res.send(success(false));
							}
							else
								res.send(success(true));  
						});
					}
					else res.send(success(false, "Invalid token"));
				});

		})
});

app.get('/village/getPlayers', function(req, res) {
	var villageId	= parseInt(req.param('villageId'));

	mongo.players.find({"VillageId": villageId}, function(err, result) {
		if (err || !result)
			res.send(success(false));
		else
			res.send(result);
	});
});

app.get('/village/getVillage', function(req, res) {
	var sourceUserId	= parseInt(req.param('sourceUserId'));

	if(sourceUserId){
		getPlayerById(sourceUserId, function(sourcePlayer){
			if(sourcePlayer){
				mongo.villages.findOne({"Id": sourcePlayer.VillageId}, function(err, result) {
					if (err || !result)
						res.send(success(false, "Village doesn't exist"));
					else
						res.send(result)});				
			}
			else{
				res.send(success(false, "Player not found"));
				return;}
		});
	}
	else{
		res.send(success(false, "Incomplete request"));
		return;}

	});



app.post('/village/setPosition', function(req, res) {
	var sourceUserId	= parseInt(req.param('sourceUserId'));
	var targetName		= req.param('targetName');
	var villageId      	= parseInt(req.param('villageId'));
	var position 		= parseInt(req.param('position'));
	var requestToken	= req.param('requestToken');


	if (sourceUserId && targetName && villageId && position.toString() && requestToken) {
		var _token = token.calculateHMAC(sourceUserId.toString()+targetName+villageId.toString()+position.toString());
	}
	else {
		res.send(success(false, "Incomplete request"));
		return;}

		getPlayerByName(targetName, function(targetUserId)
		{
			if(!targetUserId)
			{
				res.send(success(false, "Player doesn't exist"));
				return;}
				else
					getPlayerById(sourceUserId, function(sourcePlayer){

						if(sourcePlayer.VillageId != targetUserId.VillageId){
							res.send(success(false, "This player doesn't belong to this village"));
							return;
						}

						if(targetUserId.Position == 1){
							res.send(success(false, "Cannot edit position of this player"));
							return;
						}		

						if(position == 1){
							res.send(success(false, "You cannot grant this position"));
							return;
						}							

						if(position < 0 || position > 4){
							res.send(success(false, "Incorrect position"));
							return;
						}
						if(_token == requestToken){
							mongo.players.update({"Id": targetUserId.Id}, { $set: { "Position": position } }, function(err, result) {
								if (err || !result) {
									res.send(success(false));
								}
								else
									res.send(success(true));  
							});
						}
						else res.send(success(false, "Invalid token"));
					});

			})

});

app.post('/village/removePlayer', function(req, res) {
	var sourceUserId	= parseInt(req.param('sourceUserId'));
	var targetName		= req.param('targetName');
	var requestToken	= req.param('requestToken');

	if (sourceUserId && targetName && requestToken) {
		var _token = token.calculateHMAC(sourceUserId.toString()+targetName);
	}
	else {
		res.send(success(false, "Incomplete request"));
		return;
	}	

	getPlayerByName(targetName, function(targetUserId)
	{
		if(!targetUserId)
		{
			res.send(success(false, "Player doesn't exist"));
			return;}
			else
				getPlayerById(sourceUserId, function(sourcePlayer){

					if(sourcePlayer.VillageId != targetUserId.VillageId){
						res.send(success(false, "This player doesn't belong to this village"));
						return;
					}

					if(targetUserId.Position == 1){
						res.send(success(false, "Cannot remove this player"));
						return;
					}	

					if(_token == requestToken){
						mongo.players.update({"Id": targetUserId.Id}, { $set: { "VillageId": 0, "Position": 0 } }, function(err, result) {
							if (err || !result) {
								res.send(success(false));
							}
							else
								res.send(success(true));  
						});
					}
					else res.send(success(false, "Invalid token"));
				});

		})
});

app.get('/testConnection', function(req, res) {
	res.send("OK");
	});