var searchData=
[
  ['panel',['panel',['../class_u_i_draggable_panel.html#a589d390161960f6466831b4f30bd90a6',1,'UIDraggablePanel.panel()'],['../class_u_i_widget.html#a4b4ab6a8a66696a91cf05bed017760cd',1,'UIWidget.panel()']]],
  ['partialsprite',['partialSprite',['../class_n_g_u_i_settings.html#ad1c2bd2a9cb1e8e7b12af79a64e09a36',1,'NGUISettings']]],
  ['password',['password',['../class_u_i_label.html#a403160dd5b1954d1d07c76c5166f187c',1,'UILabel']]],
  ['pivot',['pivot',['../class_n_g_u_i_settings.html#a535262ea2110b1907215543fd5b9187b',1,'NGUISettings.pivot()'],['../class_u_i_widget.html#a2ee70457509eefc06c89e9b91b3f0591',1,'UIWidget.pivot()']]],
  ['pivotoffset',['pivotOffset',['../class_u_i_widget.html#a8e2c11807cbf3b82803c00f06d35df5e',1,'UIWidget.pivotOffset()'],['../class_u_i_sliced_sprite.html#ad217f129d897be087483d52add1fc991',1,'UISlicedSprite.pivotOffset()'],['../class_u_i_sprite.html#ac93343183aa12c1862b57dc5d9675e8a',1,'UISprite.pivotOffset()']]],
  ['pixelsize',['pixelSize',['../class_u_i_atlas.html#a702080aace67efe1b721c46f0991f4b3',1,'UIAtlas.pixelSize()'],['../class_u_i_font.html#ab428af2ce89100ca398376afb8ad71f0',1,'UIFont.pixelSize()']]],
  ['pixelsizeadjustment',['pixelSizeAdjustment',['../class_u_i_root.html#a5d0859d909705374f0081dcb4a984ac4',1,'UIRoot']]],
  ['premultipliedalpha',['premultipliedAlpha',['../class_u_i_atlas.html#aca4b54847df0960b73e0e922bb309fb7',1,'UIAtlas.premultipliedAlpha()'],['../class_u_i_font.html#acb3acccdedf17e18207ed31d059b0e58',1,'UIFont.premultipliedAlpha()'],['../class_u_i_texture.html#a94ca5bd16f7f3e360396aaa9672af3d7',1,'UITexture.premultipliedAlpha()']]],
  ['previousselection',['previousSelection',['../class_n_g_u_i_editor_tools.html#a9de7421267f0e7c369507910c2b6bfd1',1,'NGUIEditorTools']]],
  ['processedtext',['processedText',['../class_u_i_label.html#afaffe92d1099cb1a1e0f1d6e06cc3a62',1,'UILabel']]]
];
