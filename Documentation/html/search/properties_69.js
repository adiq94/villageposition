var searchData=
[
  ['inneruv',['innerUV',['../class_u_i_sliced_sprite.html#a235df360d0a47a2d9491516223d3e6bf',1,'UISlicedSprite']]],
  ['instance',['instance',['../class_localization.html#a80bccf2b05029d0cb21151173221f328',1,'Localization']]],
  ['invert',['invert',['../class_u_i_filled_sprite.html#a2e5ac8ec3786c537d0c7f89110349a01',1,'UIFilledSprite']]],
  ['inverted',['inverted',['../class_u_i_scroll_bar.html#a775667f4b755eaa6acb646c037e1ba36',1,'UIScrollBar']]],
  ['ischecked',['isChecked',['../class_u_i_checkbox.html#a955510f501df80fecac99d38533dcc35',1,'UICheckbox']]],
  ['isenabled',['isEnabled',['../class_u_i_button.html#a377c37278f2412a146e99b7f4a90324c',1,'UIButton']]],
  ['isopen',['isOpen',['../class_u_i_popup_list.html#a359eb61725ab1d61d90a751492d41df2',1,'UIPopupList']]],
  ['isplaying',['isPlaying',['../class_active_animation.html#afdee2db0f7af94bf5c4d9958d658d001',1,'ActiveAnimation.isPlaying()'],['../class_u_i_sprite_animation.html#aac92b5f7c772ea71bfb2249e418d1ecf',1,'UISpriteAnimation.isPlaying()']]],
  ['isvalid',['isValid',['../class_b_m_font.html#a0acc8b6af6e41b819cfea4cd2be369ab',1,'BMFont.isValid()'],['../class_u_i_sprite.html#af8425939300e29ade7e7ddd93c8a5403',1,'UISprite.isValid()']]],
  ['isvisible',['isVisible',['../class_u_i_widget.html#af19a35ace35e856fbf322230d38cd98a',1,'UIWidget']]],
  ['items',['items',['../class_u_i_item_storage.html#afb8ed34e1d4f40bb48aecba623352d6f',1,'UIItemStorage']]]
];
