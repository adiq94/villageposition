var searchData=
[
  ['backdroptexture',['backdropTexture',['../class_n_g_u_i_editor_tools.html#a93b4625db4c36e6e82b1ddebabff5901',1,'NGUIEditorTools']]],
  ['background',['background',['../class_u_i_scroll_bar.html#a04d02ed58f0c8c95b713fdd00ab60668',1,'UIScrollBar']]],
  ['barsize',['barSize',['../class_u_i_scroll_bar.html#ab81b39a693d9d1be15bff04f70a51f3b',1,'UIScrollBar']]],
  ['baseitem',['baseItem',['../class_inv_game_item.html#a09a08ce3bd8454b415bb7900313a94dd',1,'InvGameItem']]],
  ['baseitemid',['baseItemID',['../class_inv_game_item.html#add0f5894fca51f2c6127ea31af7af036',1,'InvGameItem']]],
  ['baseoffset',['baseOffset',['../class_b_m_font.html#a916531ff5c076792b85d2ee8079cb763',1,'BMFont']]],
  ['blanktexture',['blankTexture',['../class_n_g_u_i_editor_tools.html#a7640a59a33528fd58e7b82fe32c7d86f',1,'NGUIEditorTools']]],
  ['bmfont',['bmFont',['../class_u_i_font.html#af30779b9908c7f46ab5c8bf505954d9e',1,'UIFont']]],
  ['border',['border',['../class_u_i_sliced_sprite.html#a766a823f4bb6eb4f9a5d7798b9f19a40',1,'UISlicedSprite.border()'],['../class_u_i_sprite.html#a3ba7478a2d906bdb073fa1e96845b8d3',1,'UISprite.border()'],['../class_u_i_tiled_sprite.html#a52ea23d3ce7f07df486f9942729a90cf',1,'UITiledSprite.border()']]],
  ['bounds',['bounds',['../class_u_i_draggable_panel.html#a22fb0a49010926ebf67cc556da48f637',1,'UIDraggablePanel']]]
];
