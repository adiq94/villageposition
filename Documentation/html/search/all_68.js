var searchData=
[
  ['halfpixeloffset',['halfPixelOffset',['../class_u_i_anchor.html#ae6bf43be90ebc26aa7f13fd43eaa5aea',1,'UIAnchor']]],
  ['haschanged',['HasChanged',['../class_u_i_node.html#a9e00977230a7d2e85516a9f9fe8664c2',1,'UINode']]],
  ['hasdynamicmaterial',['hasDynamicMaterial',['../class_u_i_texture.html#ae7e6cd0504c0a19540ea33b7294aa946',1,'UITexture']]],
  ['hasequipped',['HasEquipped',['../class_inv_equipment.html#a61c3c3d9ba4a9bd6ff8bfe03f6f20ee4',1,'InvEquipment.HasEquipped(InvGameItem item)'],['../class_inv_equipment.html#a568bc66a35d29830e6b21cc07ee3f974',1,'InvEquipment.HasEquipped(InvBaseItem.Slot slot)']]],
  ['haspreviewgui',['HasPreviewGUI',['../class_u_i_atlas_inspector.html#aaf0aa52a30ff4dd4090f4b08d3ed2b9c',1,'UIAtlasInspector.HasPreviewGUI()'],['../class_u_i_sprite_inspector.html#a42ecee386b87f3596aeea307ea1c2f93',1,'UISpriteInspector.HasPreviewGUI()']]],
  ['hastransformed',['hasTransformed',['../class_u_i_geometry.html#a922c3e7cd5248c90cfdee25ff5e403b1',1,'UIGeometry']]],
  ['hasvertices',['hasVertices',['../class_u_i_geometry.html#ad4172abd918d0d472fae43f1872a487e',1,'UIGeometry']]],
  ['highlightcolor',['highlightColor',['../class_u_i_popup_list.html#ae607c22ae153794f43ae1f577a40567e',1,'UIPopupList']]],
  ['highlightline',['HighlightLine',['../class_n_g_u_i_editor_tools.html#a7807c41d2450801e05319ed7d468f19b',1,'NGUIEditorTools']]],
  ['highlightsprite',['highlightSprite',['../class_u_i_popup_list.html#a17df67dffa5997365d1da4635878e0ae',1,'UIPopupList']]],
  ['horizontalaxisname',['horizontalAxisName',['../class_u_i_camera.html#ac18ef6e9e6834d224cd2c5bdddb368fe',1,'UICamera']]],
  ['horizontalscrollbar',['horizontalScrollBar',['../class_u_i_draggable_panel.html#a85807730cba9acd56cf1d31807ab620d',1,'UIDraggablePanel']]],
  ['horizontalspacing',['horizontalSpacing',['../class_u_i_font.html#acf2db9a05a7e61a18ef299093ddb769e',1,'UIFont']]],
  ['hover',['hover',['../class_u_i_button_color.html#ae579f4148d3e94f75b45a9dcedd2f9f6',1,'UIButtonColor']]],
  ['hoveredobject',['hoveredObject',['../class_u_i_camera.html#a160bd9246c107025608f9f19147a64e7',1,'UICamera']]]
];
