var searchData=
[
  ['iconatlas',['iconAtlas',['../class_inv_base_item.html#a47f0074438550c2f587ac93dde9cc526',1,'InvBaseItem.iconAtlas()'],['../class_inv_database.html#ac91d85d3b5a4ce11e0c06f79532515b2',1,'InvDatabase.iconAtlas()']]],
  ['iconname',['iconName',['../class_inv_base_item.html#a929f74a3c224b80404f61463b24fd8da',1,'InvBaseItem']]],
  ['id16',['id16',['../class_inv_base_item.html#a955588e196071ad72312496af4875086',1,'InvBaseItem']]],
  ['ifdisabledonplay',['ifDisabledOnPlay',['../class_u_i_button_play_animation.html#a10c8680fff7fdc117a68ece19c2183d4',1,'UIButtonPlayAnimation.ifDisabledOnPlay()'],['../class_u_i_button_tween.html#a9ff803408235a3057973f0669d42c250',1,'UIButtonTween.ifDisabledOnPlay()']]],
  ['ignoretimescale',['ignoreTimeScale',['../class_spring_position.html#a96740a851144b764a6cef9ce5f5eeb2a',1,'SpringPosition.ignoreTimeScale()'],['../class_u_i_tweener.html#a70109184cbfb855672178546c041f978',1,'UITweener.ignoreTimeScale()']]],
  ['includechildren',['includeChildren',['../class_u_i_button_tween.html#a1d30969b4e911b4c9a92955af80427a4',1,'UIButtonTween']]],
  ['inputhasfocus',['inputHasFocus',['../class_u_i_camera.html#a4d742ded3a5266465a0a4b225cca315c',1,'UICamera']]],
  ['isanimated',['isAnimated',['../class_u_i_popup_list.html#add6d18f6d3aeb77aaf506d6c206674a8',1,'UIPopupList']]],
  ['islocalized',['isLocalized',['../class_u_i_popup_list.html#a716f2a347d6c309ad0f3c17ec4ed767e',1,'UIPopupList']]],
  ['ispassword',['isPassword',['../class_u_i_input.html#a290eecff15358cb98d8d8f32a4ec49e5',1,'UIInput']]],
  ['itemlevel',['itemLevel',['../class_inv_game_item.html#aa132ff16c490b26fdef58480f31359ef',1,'InvGameItem']]],
  ['items',['items',['../class_inv_database.html#a1d7c659119b09e06a345b9ee2a77a12c',1,'InvDatabase.items()'],['../class_u_i_popup_list.html#a92913c14d5b192e19e8ec651d5aad2e6',1,'UIPopupList.items()']]]
];
