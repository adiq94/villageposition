var searchData=
[
  ['activeheight',['activeHeight',['../class_u_i_root.html#a045aee89e3946173878292d82c02ee4c',1,'UIRoot']]],
  ['alpha',['alpha',['../class_u_i_scroll_bar.html#a16efd02de41c515a187537b3c78f9ba0',1,'UIScrollBar.alpha()'],['../class_u_i_widget.html#a73e17871ba3b038fe2f3b310ef0653dd',1,'UIWidget.alpha()'],['../class_tween_alpha.html#a385488b5fbdb0fc84e4818de499554fe',1,'TweenAlpha.alpha()'],['../class_u_i_panel.html#aae49c282dda63bff0206236dd3ec5b74',1,'UIPanel.alpha()']]],
  ['amountperdelta',['amountPerDelta',['../class_u_i_tweener.html#a5c2636d93c024c9172ec7ee3488ba9ee',1,'UITweener']]],
  ['atlas',['atlas',['../class_n_g_u_i_settings.html#a607c48e58f38c6b9b924dacfa3511ac3',1,'NGUISettings.atlas()'],['../class_u_i_font.html#a824851bcacb2118a318481f80514ebe2',1,'UIFont.atlas()'],['../class_u_i_sprite.html#a4dd410e8f4c3f97539284feac2c02ac7',1,'UISprite.atlas()']]],
  ['atlasname',['atlasName',['../class_n_g_u_i_settings.html#a41e55b3ed5200f6a88a90a6636c1e3ee',1,'NGUISettings']]],
  ['atlaspadding',['atlasPadding',['../class_n_g_u_i_settings.html#a279fbf009fa06c5b0ebd368c20c86859',1,'NGUISettings']]],
  ['atlastrimming',['atlasTrimming',['../class_n_g_u_i_settings.html#a7c23348148e1235fabab3004d4512cab',1,'NGUISettings']]],
  ['audiosource',['audioSource',['../class_tween_volume.html#ad8491e3b3f58414f924d5baa2cf60587',1,'TweenVolume']]]
];
