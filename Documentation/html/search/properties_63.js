var searchData=
[
  ['cachedcamera',['cachedCamera',['../class_u_i_scroll_bar.html#adc9dba36a6fb5a6e985b6c761f14c00c',1,'UIScrollBar.cachedCamera()'],['../class_tween_f_o_v.html#a84e1203e56191e70d1ad849c3914b23a',1,'TweenFOV.cachedCamera()'],['../class_tween_ortho_size.html#a9ed11b30d94ac8daa030a5267bd59162',1,'TweenOrthoSize.cachedCamera()'],['../class_u_i_camera.html#a26ed6d2c8ff236c0280058ad253fa66b',1,'UICamera.cachedCamera()']]],
  ['cachedtransform',['cachedTransform',['../class_u_i_scroll_bar.html#aa9b3ee3cbd35c9bb5aa5b3f222800859',1,'UIScrollBar.cachedTransform()'],['../class_u_i_draw_call.html#a71f53ce17ce87c29f8b1b02aeaa87c37',1,'UIDrawCall.cachedTransform()'],['../class_u_i_widget.html#a04867f73926f7c7fa51dcec060dbeae2',1,'UIWidget.cachedTransform()'],['../class_u_i_panel.html#a135a9cecbf479472b302c8399232ce81',1,'UIPanel.cachedTransform()']]],
  ['canread',['canRead',['../class_byte_reader.html#a8272dad971411ca47e3ff9347e80f711',1,'ByteReader']]],
  ['centeredobject',['centeredObject',['../class_u_i_center_on_child.html#a5afc807f98e560382299d67cdb672132',1,'UICenterOnChild']]],
  ['changedlastframe',['changedLastFrame',['../class_u_i_panel.html#ab41bcdca28a82879ccbdb71690989ae6',1,'UIPanel']]],
  ['charsize',['charSize',['../class_b_m_font.html#af790da3d41a81e09c5391fff8d80daf4',1,'BMFont']]],
  ['children',['children',['../class_u_i_table.html#ad91ed1784f82b1a74b22d084d3577c48',1,'UITable']]],
  ['clipping',['clipping',['../class_u_i_draw_call.html#a89dcb2cc0787c5c4409666133acaaf5d',1,'UIDrawCall.clipping()'],['../class_u_i_panel.html#ac9d8fe05897b650cf36d64b6b541be36',1,'UIPanel.clipping()']]],
  ['cliprange',['clipRange',['../class_u_i_draw_call.html#a368aa960b53da7123890c669247a7e46',1,'UIDrawCall.clipRange()'],['../class_u_i_panel.html#ae5c9c452fd4b23d544c77f1dd5defca0',1,'UIPanel.clipRange()']]],
  ['clipsoftness',['clipSoftness',['../class_u_i_draw_call.html#ab4173d7fa52c7c9d3643a003c5941bf9',1,'UIDrawCall.clipSoftness()'],['../class_u_i_panel.html#a724d97450cc789f2f5e211499bcce6fa',1,'UIPanel.clipSoftness()']]],
  ['color',['color',['../class_inv_game_item.html#a18d6e3625e4483ee268d6723c90c98a1',1,'InvGameItem.color()'],['../class_n_g_u_i_settings.html#a6e858820346d665bc7a00bc795fae686',1,'NGUISettings.color()'],['../class_u_i_widget.html#a70d22f68e64f19372b1eaab899abf975',1,'UIWidget.color()'],['../class_tween_color.html#ade2cbc965123a95b77a52a403be7bc9b',1,'TweenColor.color()']]],
  ['contrasttexture',['contrastTexture',['../class_n_g_u_i_editor_tools.html#ae614ecaf79a8dabf15e615bdfb75744f',1,'NGUIEditorTools']]],
  ['coordinates',['coordinates',['../class_u_i_atlas.html#a592e254b529d92ba9ae9cbd9ed6cbf2f',1,'UIAtlas']]],
  ['currentlanguage',['currentLanguage',['../class_localization.html#af41888a8b32745a3ef12c4a89187f141',1,'Localization']]],
  ['currentmomentum',['currentMomentum',['../class_u_i_draggable_camera.html#a91973f8d88f6e524049be79d913ac7fe',1,'UIDraggableCamera.currentMomentum()'],['../class_u_i_draggable_panel.html#a96d165810be7418a46b25ffd82a18af0',1,'UIDraggablePanel.currentMomentum()']]]
];
