var searchData=
[
  ['removeplayer',['RemovePlayer',['../class_players.html#a3bb8440e1752beb49987c1e461be3fc7',1,'Players.RemovePlayer(Player player)'],['../class_players.html#a91e6fa12076145a2a0ce8d5a09f25973',1,'Players.RemovePlayer(string playerName)'],['../class_server_connection.html#acc0177a63daf3e4278893096f1a16dec',1,'ServerConnection.RemovePlayer()'],['../class_village.html#afb962756c0af324c22d0b6bd9cd69d48',1,'Village.RemovePlayer()']]],
  ['response',['Response',['../class_village.html#af5611d772ae7abdc732919f080737aaa',1,'Village']]],
  ['responsehandler',['ResponseHandler',['../class_village.html#ad03bd5b6ce7e589170c6dcc23b646734',1,'Village']]]
];
