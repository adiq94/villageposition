var searchData=
[
  ['watchestransform',['WatchesTransform',['../class_u_i_panel.html#a500e35ef5afd328875b1debbd5fd3e2d',1,'UIPanel']]],
  ['willloseprefab',['WillLosePrefab',['../class_n_g_u_i_editor_tools.html#aefc6ab99cd49f2d38b26b65ab582cd54',1,'NGUIEditorTools']]],
  ['wraptext',['WrapText',['../class_u_i_font.html#accb6888b8f0f918f108ee4a68a4f40f7',1,'UIFont.WrapText(string text, float maxWidth, int maxLineCount, bool encoding, SymbolStyle symbolStyle)'],['../class_u_i_font.html#a2814cd96114e5770ba5836836f7b4f69',1,'UIFont.WrapText(string text, float maxWidth, int maxLineCount, bool encoding)'],['../class_u_i_font.html#a283c8648c5292ccb5f7d8c47f88765eb',1,'UIFont.WrapText(string text, float maxWidth, int maxLineCount)']]],
  ['writetobuffers',['WriteToBuffers',['../class_u_i_geometry.html#a81f4518370f1ec96238d1b5ee44bd5db',1,'UIGeometry.WriteToBuffers()'],['../class_u_i_widget.html#a178c63f7db7e382f595fb4349c45dd0e',1,'UIWidget.WriteToBuffers()']]]
];
