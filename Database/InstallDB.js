db.villages.insert({
  Id: 1,
  Name: 'Wioska'
});

db.players.insert({
  Id: 1,
  Name: 'OurPlayer',
  VillageId: 1,
  Position: 1
});

db.players.insert({
  Id: 2,
  Name: 'User01',
  VillageId: 1,
  Position: 2
});

db.players.insert({
  Id: 3,
  Name: 'User02',
  VillageId: 1,
  Position: 3
});

db.players.insert({
  Id: 4,
  Name: 'User03',
  VillageId: 1,
  Position: 4
});

db.players.insert({
  Id: 5,
  Name: 'User04',
  VillageId: 1,
  Position: 5
});

db.players.insert({
  Id: 6,
  Name: 'UserWithoutVillage',
  VillageId: 0,
  Position: 0
});

